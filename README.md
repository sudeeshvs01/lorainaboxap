# LorainaboxAP

Simple AP setup & WiFi management for the Raspberry Pi 

This project is a part of LorainaBox project by ICFOSS

A simple, responsive web interface to control wifi, hostapd and related services on the Raspberry Pi.

This project was inspired by https://github.com/billz/raspap-webgui

 LorainaboxAP lets you easily create an AP with a Wifi client configuration.
 With your RPi configured in managed mode, enable the AP from the Advanced tab of Configure hotspot by sliding the Wifi client AP mode toggle.
 Save settings and start the hotspot. The managed mode AP is functional without restart.
 
 LorainaboxAP  Help to configure your gateway /network server via a Wifi AP.

TO INSTALL

Recommended Raspberry Pi 3,Raspberry Pi 3 B+,Raspberry Pi 4 and OS are Raspbian Buster,Raspbian Stretch 

1.CLONE THE REPOSITORY

https://gitlab.com/sudeeshvs01/lorainaboxap.git

2.add permissions

chmod +x -R lorainaboxap

3.change directory 

cd lorainaboxap/installers

4.install 

sudo ./install.sh


After the reboot at the end of the installation the wireless network will be configured as an access point as follows:

#************************************************************************
   
   
    IP address: 192.168.50.1
    
    
        Username: admin
        
        
        Password: secret
        
        
    DHCP range: 192.168.50.5 to 192.168.50.200
    
    
    SSID: LorainaboxAP
    
    
    Password:12344567888
    
    

#***********************************************************************