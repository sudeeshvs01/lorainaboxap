<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-heading"><i class="fa fa-info-circle fa-fw"></i> <?php echo _("About Lora in a Box AP"); ?></div>
      <div class="panel-body text-center">

        <h3><?php echo _("Lora in a Box AP") . " v" . RASPI_VERSION; ?></h3>
        <h5>GNU General Public License v3.0</h5>
      
        <p>Lora in a Box AP is a co-creation of <a href="https://gitlab.com/icfoss">@ICFOSS</a> 
        

      </div><!-- /.panel-body -->
      <div class="panel-footer"></div>
    </div><!-- /.panel-primary -->
  </div><!-- /.col-lg-12 -->
</div><!-- /.row -->
