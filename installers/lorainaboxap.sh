
cp $(pwd)/common.sh  /tmp/lorainaboxapcommon.sh
#chmod 777 /tmp/lorainaboxapcommon.sh
source /tmp/lorainaboxapcommon.sh 

function update_system_packages() {
    install_log "Updating sources"
    sudo apt-get update || install_error "Unable to update package list"
}

function install_dependencies() {
    install_log "Installing required packages"
    sudo apt-get install lighttpd $php_package git hostapd dnsmasq vnstat || install_error "Unable to install dependencies"
}

install_lorainaboxap
